const MonacoWebpackPlugin = require('monaco-editor-webpack-plugin');
const pkg = require('./package');

module.exports = {
    apiPath: 'stubs/api',
    webpackConfig: {
        output: {
            publicPath: `/static/${pkg.name}/${
                process.env.VERSION || pkg.version
            }/`,
        },
        plugins: [
            new MonacoWebpackPlugin({
                // languages: ['javascript', 'markdown']
            }),
        ],
    },
    navigations: {
        'main.main': '/main',
        'main.new.answer': '/task/:taskId',
        'main.edit.answer': '/task/:taskId/:answerId',
        'main.create.task': '/create',
        'main.edit.task': '/edit/:taskId',
    },
    features: {
        main: {
            // add your features here in the format [featureName]: { value: string }
            'main.banner': {
                value: '1',
            },
            'auth.redirect': {
                on: true,
                value: 'auth.main',
                key: 'auth.redirect',
            },
        },
    },
    config: {
        'main.red.coder.api': '/api',
        // 'main.red.coder.api': 'http://localhost:8045',
    },
};
