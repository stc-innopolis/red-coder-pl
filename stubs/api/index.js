const router = require('express').Router();
const { v4: uuid } = require('uuid');

const wait = (req, res, next) => setTimeout(next, 1500);

router.get('/banner-data', wait, (req, res) => {
    res.send(require('./json/banner/success.json'));
});

router.get('*/drafts', wait, (req, res) => {
    res.send(require('./json/tasks/success.json'))
})

router.get('*/tasks', wait, wait, (req, res) => {
    res.send(require('./json/tasks/success.json'));
});

router.get('*/tasks/create', wait, (req, res) => {
    res.send({ id: uuid() });
});

router.post('*/task/data', wait, (req, res) => {
    res.send({});
});

module.exports = router;
