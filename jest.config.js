module.exports = {
    clearMocks: true,
    collectCoverage: true,
    collectCoverageFrom: ['src/*', 'src/**/*', '!src/index.tsx'],
    coverageDirectory: '<rootDir>/reports/coverage',
    coverageProvider: 'v8',
    coverageReporters: [
        [
            'html',
            {
                subdir: 'html',
            },
        ],
    ],
    moduleNameMapper: {
        i18next: '<rootDir>/__mocks__/i18next.js',
        'react-i18next': '<rootDir>/__mocks__/i18next.js',
        '@ijl/cli': '<rootDir>/__mocks__/ijl-cli.js',
    },
    preset: 'ts-jest',
    testEnvironment: 'jsdom',
    transform: {
        '\\.[tj]sx&?$': './transformer.js',
        '\\.(jpg|jpeg|png|gif|eot|otf|webp|svg|ttf|woff|woff2|mp4|webm|wav|mp3|m4a|aac|oga)$':
            '<rootDir>/fileTransformer.js',
    },
};
