import React, { Suspense } from 'react';
import { Routes, Route } from 'react-router-dom';

import { URLs } from './__data__/urls';
import { ErrorBoundary, Lazy } from './components';

import './style';
import { MainPage, AnswerPage, EditTask } from './pages';

const Dashboard = () => {
    return (
        <ErrorBoundary>
            <Routes>
                <Route
                    path={URLs.dashboard.url}
                    element={
                        <Lazy>
                            <MainPage />
                        </Lazy>
                    }
                />

                <Route
                    path={URLs.newAnswer.url}
                    element={
                        <Lazy>
                            <AnswerPage />
                        </Lazy>
                    }
                />
                <Route
                    path={URLs.editAnswer.url}
                    element={
                        <Lazy>
                            <AnswerPage />
                        </Lazy>
                    }
                />

                <Route
                    path={URLs.createTask.url}
                    element={
                        <Lazy>
                            <EditTask />
                        </Lazy>
                    }
                />
                <Route
                    path={URLs.editTask.url}
                    element={
                        <Lazy>
                            <EditTask />
                        </Lazy>
                    }
                />

                <Route path="*" element={<h1>Not Found</h1>} />
            </Routes>
        </ErrorBoundary>
    );
};

export default Dashboard;
