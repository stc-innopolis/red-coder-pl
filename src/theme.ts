export const theme = {
    colors: {
        text: {
            main: '#ffffff',
        },
        bg: {
            main: '#000000',
        },
        primary: {
            main: '#3576CB',
            dark: '#0E242F',
            dirt: '#ACCEDF',
            light: '#D7EEFA',
        },
        action: {
            main: '#67AC5B',
        },
        accent: {
            main: '#EB3AEF',
        },
        bingo: {
            main: '#FFD90F',
        },
        dark: {
            main: '#0D0C22',
        },
        gray: {
            main: '#888888',
        },
        promo: {
            main: '#EC5E5E',
        },
        error: {
            main: '#DE5E56',
            dirt: '#DFACAC',
            light: '#FAD7D7',
        },
    },
};
