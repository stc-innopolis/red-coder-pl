import React from 'react';
import { Router } from 'react-router-dom';
import { getHistory } from '@ijl/cli';
import { ThemeProvider } from '@emotion/react';
import { Provider } from 'react-redux';

import Dashboard from './dashboard';
import { store } from './__data__/store';
import { theme } from './theme';

const history = getHistory();

const App = () => {
    return (
        <Provider store={store}>
            <ThemeProvider theme={theme}>
                <Router location={history.location} navigator={history}>
                    <Dashboard />
                </Router>
            </ThemeProvider>
        </Provider>
    );
};

export default App;
