import { useEffect } from 'react';
import { useNavigate } from 'react-router-dom';
import { getFeatures, getNavigationsValue } from '@ijl/cli';

const useProtectedPage = () => {
    const navigate = useNavigate();

    useEffect(() => {
        const token = localStorage.getItem('red-coder-token');

        if (!token) {
            navigate(
                getNavigationsValue(getFeatures('main')['auth.redirect']?.value)
            );
        }
    }, []);
};
