import { useCallback, useRef, useEffect } from 'react';
import { editor as mEditor } from 'monaco-editor';

export const useMonako = (
    inputData: string,
    remountFlag: unknown,
    language: string,
    onChange: (value: string) => void = () => null
) => {
    const editorRef = useRef<mEditor.IStandaloneCodeEditor>();
    const nodeRef = useRef(null);
    const remount = useCallback(() => {
        editorRef.current?.dispose();
        editorRef.current = mEditor.create(nodeRef.current, {
            value: inputData,
            theme: 'light' === 'light' ? 'vs' : 'vs-dark',
            language,
        });

        const model = editorRef.current.getModel();
        model.onDidChangeContent(() => {
            const value = model.getValue();
            onChange(value);
        });
    }, [inputData, nodeRef]);

    useEffect(() => {
        try {
            setTimeout(() => {
                if (nodeRef.current) {
                    remount();

                    () => {
                        editorRef.current.dispose();
                        editorRef.current = null;
                    };
                }
            }, 2);
        } catch (error) {
            console.error(error);
        }
    }, [nodeRef, remountFlag]);

    return {
        nodeRef,
        getValue: () => editorRef?.current?.getValue(),
    };
};
