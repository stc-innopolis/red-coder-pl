export type Request<T> = {
    success?: boolean;
    data?: T;
    error?: string;
};
