export type TaskShort = {
    number: number;
    id: string;
    names: Names;
    rating: number;
    resolveCount: number;
    views: number;
    lastModufy: number;
};

type Names = {
    ru: string;
    en: string;
};
