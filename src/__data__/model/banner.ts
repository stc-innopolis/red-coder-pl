export type BannerData = {
    title: string;
    body: string;
    icon: string;
};
