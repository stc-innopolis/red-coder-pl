import { createSlice } from '@reduxjs/toolkit';

const initialState = {
    showBanner: true,
};

const slice = createSlice({
    name: 'bannerLogic',
    initialState,
    reducers: {
        hide: (state) => {
            state.showBanner = false;
        },
    },
});

const { reducer: bannerReducer, actions } = slice;

export { bannerReducer, actions };
