import { configureStore } from '@reduxjs/toolkit';

import { bannerReducer } from './slices/banner-logic';
import { api } from '../services/api';

export const store = configureStore({
    reducer: {
        banner: bannerReducer,
        [api.reducerPath]: api.reducer,
    },
    middleware: (defaultMDW) => defaultMDW().concat(api.middleware),
});

export type StoreType = ReturnType<typeof store.getState>;
