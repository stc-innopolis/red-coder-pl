import { getNavigationsValue } from '@ijl/cli';

const baseUrl = getNavigationsValue('main.main');

export const URLs = {
    dashboard: {
        url: `${baseUrl}`,
    },
    newAnswer: {
        url: `${baseUrl}${getNavigationsValue('main.new.answer')}`,
        getUrl(taskId) {
            return `${baseUrl}${getNavigationsValue(
                'main.new.answer'
            )}`.replace(':taskId', taskId);
        },
    },
    editAnswer: {
        url: `${baseUrl}${getNavigationsValue('main.edit.answer')}`,
        getUrl(taskId, answerId) {
            return `${baseUrl}${getNavigationsValue('main.edit.answer')}`
                .replace(':taskId', taskId)
                .replace(':answerId', answerId);
        },
    },
    createTask: {
        url: `${baseUrl}${getNavigationsValue('main.create.task')}`,
    },
    editTask: {
        url: `${baseUrl}${getNavigationsValue('main.edit.task')}`,
        getUrl(taskId) {
            return `${baseUrl}${getNavigationsValue('main.edit.task')}`.replace(
                ':taskId',
                taskId
            );
        },
    },
};
