import React from 'react';
import styled from '@emotion/styled';
import { Theme } from '@emotion/react';

type ButtonProps = {
    colorSheme: 'red' | 'green' | 'blue';
    type?: 'button' | 'submit';
} & React.DetailedHTMLProps<
    React.ButtonHTMLAttributes<HTMLButtonElement>,
    HTMLButtonElement
>;

const getBackground = (colorSheme: 'red' | 'green' | 'blue', theme: Theme) => {
    switch (colorSheme) {
    case 'green':
        return theme.colors.action.main;
    default:
        return theme.colors.action.main;
    }
};

const StyledButton = styled.button<ButtonProps>(
    ({ colorSheme, theme }) => `
    padding: 11px 10px;
    background: ${getBackground(colorSheme, theme)};
    border: none;
    color: white;
    border-radius: 4px;
`
);

export const Button: React.FC<ButtonProps> = ({ children, ...rest }) => {
    return <StyledButton {...rest}>{children}</StyledButton>;
};

Button.defaultProps = {
    type: 'button',
};
