import React from 'react';

import { Header, HeaderProps } from '../header';

export const PageWrapper: React.FC<HeaderProps> = ({ children, ...rest }) => {
    return (
        <>
            <Header {...rest} />
            {children}
        </>
    );
};
