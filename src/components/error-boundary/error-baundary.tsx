import React from 'react';
import { Banner } from '../banner';

type ErrorBaoundaryState = {
    error: string;
};

type ErrorBaoundaryProps = {
    onClick?: () => void;
};

export default class ErrorBoundary extends React.Component<
    ErrorBaoundaryProps,
    ErrorBaoundaryState
> {
    state = {
        error: null,
    };

    static getDerivedStateFromError(error) {
        return {
            error: error.message,
        };
    }

    static defaultProps = {
        onClick: () => location.reload(),
    };

    render() {
        if (this.state.error) {
            return (
                <Banner
                    title="Тут явно что-по пошло не так как задумывалось"
                    body="Разработчики уже решают эту проблему"
                    icon="fatalError"
                    mode="error"
                    onAction={this.props.onClick}
                    actionTitle="Перезагрузить"
                />
            );
        }
        return this.props.children;
    }
}
