import React from 'react';
import ReactMarkdown from 'react-markdown';

import * as C from './styles';

export const Markdown = ({ markdown }) => {
    return (
        <ReactMarkdown
            components={{
                h1: ({ node, ...props }) => <C.H1 {...props} />,
                h2: ({ node, ...props }) => <C.H2 {...props} />,
                h3: ({ node, ...props }) => <C.H3 {...props} />,
                h4: ({ node, ...props }) => <C.H4 {...props} />,
                h5: ({ node, ...props }) => <C.H5 {...props} />,
                h6: ({ node, ...props }) => <C.H6 {...props} />,
                p: ({ node, ...props }) => <C.P {...props} />,
                a: ({ node, href, ...props }) =>
                    /^http/.test(href) ? (
                        <a
                            target="_blank"
                            rel="noreferrer noopener"
                            href={href}
                            {...props}
                        />
                    ) : (
                        <C.Link to={href} {...props} />
                    ),
                img: ({ node, ...props }) => (
                    <C.ImageWrapper>
                        <C.Image {...props} />
                    </C.ImageWrapper>
                ),
            }}
        >
            {markdown}
        </ReactMarkdown>
    );
};
