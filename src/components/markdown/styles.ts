import styled from '@emotion/styled';
import { Link as ConnectedLink } from 'react-router-dom';

export const H1 = styled.h1`
    font-style: normal;
    font-weight: 400;
    font-size: 32px;
    line-height: 32px;
    margin: 24px 0;
`;
export const H2 = styled.h2`
    font-style: normal;
    font-weight: 400;
    font-size: 28px;
    line-height: 28px;
    margin: 18px 0;
`;
export const H3 = styled.h3`
    font-style: normal;
    font-weight: 400;
    font-size: 24px;
    line-height: 24px;
    margin: 16px 0;
`;
export const H4 = styled.h4`
    font-style: normal;
    font-weight: 400;
    font-size: 22px;
    line-height: 22px;
    margin: 14px 0;
`;
export const H5 = styled.h5`
    font-style: normal;
    font-weight: 400;
    font-size: 18px;
    line-height: 18px;
    margin: 12px 0;
`;
export const H6 = styled.h6`
    font-style: normal;
    font-weight: 400;
    font-size: 16px;
    line-height: 16px;
    margin: 8px 0;
`;
export const P = styled.p`
    font-style: normal;
    font-weight: 400;
    font-size: 16px;
    line-height: 16px;
    margin: 12px 0;
`;
export const Link = styled(ConnectedLink)`
    color: ${({ theme }) => theme.colors.accent.main};
`;

export const ImageWrapper = styled.div`
    height: 260px;
`;

export const Image = styled.img`
    margin: 0 auto;
    display: block;
    max-width: 100%;
    max-height: 100%;
`;
