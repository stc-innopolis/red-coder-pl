import styled from '@emotion/styled';
import { Link } from 'react-router-dom';

import { Button } from '../button';

export const ActionButton = styled(Button)`
    margin-right: 24px;
`;

export const Wrapper = styled.header`
    height: 64px;
    display: flex;
    align-items: center;
    background-color: ${(props) => props.theme.colors.primary.main};
`;

export const LogoText = styled(Link)`
    font-family: 'Roboto';
    font-style: normal;
    font-weight: 700;
    font-size: 36px;
    line-height: 42px;
    display: flex;
    align-items: center;
    color: #ffffff;
    text-decoration: none;
    margin-right: auto;
`;

export const RedLogoPart = styled.span(
    ({ theme }) => `
    color: ${theme.colors.promo.main};
    margin-right: 8px;
    text-decoration: none;
`
);

export const Logo = styled.img`
    margin: 0 22px 0 24px;
`;

export const Avatar = styled.img`
    margin-right: 24px;
`;

export const LogOut = styled.img`
    margin-right: 24px;
`;
