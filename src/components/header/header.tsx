import React from 'react';
import { Link } from 'react-router-dom';

import { logo } from '../../assets';
import { avatar, logout } from '../../assets/icons';
import { URLs } from '../../__data__/urls';

import {
    Wrapper,
    Logo,
    LogoText,
    RedLogoPart,
    Avatar,
    LogOut,
    ActionButton,
} from './style';

export type HeaderProps = {
    actionButtonText?: string;
    action?: () => void;
};

export const Header: React.FC<HeaderProps> = ({ actionButtonText, action }) => (
    <Wrapper>
        <Logo src={logo} alt="logo" />
        <LogoText to={URLs.dashboard.url}>
            <RedLogoPart>RED</RedLogoPart> CODER
        </LogoText>
        {actionButtonText && action && (
            <ActionButton colorSheme="green" onClick={action}>
                {actionButtonText}
            </ActionButton>
        )}
        <Link to="/auth">
            <Avatar src={avatar} alt="avatar" />
        </Link>
        <LogOut src={logout} alt="exit-icon" />
    </Wrapper>
);
