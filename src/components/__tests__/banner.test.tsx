import React from 'react';
import { describe, it, expect, jest } from '@jest/globals';
import { render, screen, fireEvent } from '@testing-library/react';
import { ThemeProvider } from '@emotion/react';

import { BannerSkeleton, Banner } from '../banner';
import { theme } from '../../theme';

describe('<Banner />', () => {
    it('Skeleton info', () => {
        render(
            <ThemeProvider theme={theme}>
                <BannerSkeleton />
            </ThemeProvider>
        );

        expect(document.body).toMatchSnapshot();
    });

    it('Skeleton error', () => {
        render(
            <ThemeProvider theme={theme}>
                <BannerSkeleton mode="error" />
            </ThemeProvider>
        );

        expect(document.body).toMatchSnapshot();
    });

    it.each(['allGood', 'askingQuestion', 'coding', 'fatalError'])(
        'Banner',
        (icon) => {
            render(
                <ThemeProvider theme={theme}>
                    <Banner
                        mode="error"
                        title="test title"
                        icon={icon}
                        body="banner body text"
                    />
                </ThemeProvider>
            );

            expect(document.body).toMatchSnapshot();
        }
    );

    it('Banner clicks', () => {
        const onNext = jest.fn();
        const onPrev = jest.fn();

        render(
            <ThemeProvider theme={theme}>
                <Banner
                    mode="info"
                    title="test title"
                    icon="allGood"
                    body="banner body text"
                    onNext={onNext}
                    onPrev={onPrev}
                />
            </ThemeProvider>
        );

        fireEvent.click(screen.getByTitle('Предыдущий слайд'));
        fireEvent.click(screen.getByTitle('Следующий слайд'));

        expect(document.body).toMatchSnapshot();
        expect(onNext).toBeCalledTimes(1);
        expect(onPrev).toBeCalled();
    });
});
