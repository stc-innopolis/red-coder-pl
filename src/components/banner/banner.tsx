import React from 'react';
import { useTranslation } from 'react-i18next';

import Typography from '../typography';
import { arrowLeft, arrowRight, close } from '../../assets/icons';
import { chars } from '../../assets';

import {
    Wrapper,
    IconButton,
    ControlsWrapper,
    CloseButton,
    Character,
    ModeProps,
    ActionButton,
    Skeleton,
    CloseButtonSkeleton,
} from './style';

type BannerProps = Partial<ModeProps> & {
    icon: keyof typeof chars;
    title: string;
    body: string;
    onClose?: () => void;
    onNext?: () => void;
    onPrev?: () => void;
    onAction?: () => void;
    actionTitle?: string;
};

export const BannerSkeleton: React.FC<Pick<BannerProps, 'mode'>> = ({
    mode,
}) => (
    <Wrapper mode={mode}>
        <Skeleton width={375} height={42} marginVertical={0} />
        <Skeleton width={600} height={19} marginVertical={24} />

        <ControlsWrapper>
            <Skeleton width={32} height={32} marginVertical={0} />
            <Skeleton
                width={32}
                height={32}
                marginVertical={0}
                style={{ marginLeft: 18 }}
            />
        </ControlsWrapper>
        <CloseButtonSkeleton
            width={32}
            height={32}
            marginVertical={0}
            style={{ marginLeft: 18 }}
        />
    </Wrapper>
);

BannerSkeleton.defaultProps = {
    mode: 'info',
};

export const Banner: React.FC<BannerProps> = ({
    icon,
    body,
    title,
    onClose,
    onNext,
    onPrev,
    mode,
    actionTitle,
    onAction,
}) => {
    const { t } = useTranslation();
    return (
        <>
            <Wrapper mode={mode}>
                <Typography.Header2 indent="zero">{title}</Typography.Header2>
                <Typography>{body}</Typography>
                <ControlsWrapper>
                    {onPrev && (
                        <IconButton
                            onClick={onPrev}
                            mode={mode}
                            title={t('red.coder.banner.controllers.prev')}
                        >
                            <img src={arrowLeft} />
                        </IconButton>
                    )}
                    {onNext && (
                        <IconButton
                            onClick={onNext}
                            mode={mode}
                            title={t('red.coder.banner.controllers.next')}
                        >
                            <img src={arrowRight} />
                        </IconButton>
                    )}
                    {actionTitle && onAction && (
                        <ActionButton mode={mode} onClick={onAction}>
                            {actionTitle}
                        </ActionButton>
                    )}
                </ControlsWrapper>
                <Character src={chars[icon]} />
                {onClose && (
                    <CloseButton onClick={onClose} mode={mode}>
                        <img src={close} />
                    </CloseButton>
                )}
            </Wrapper>
        </>
    );
};

Banner.defaultProps = {
    mode: 'info',
};
