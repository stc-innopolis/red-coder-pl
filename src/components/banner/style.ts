import styled from '@emotion/styled';

export type ModeProps = {
    mode: 'error' | 'info';
};

export const Wrapper = styled.div<ModeProps>`
    background: ${(props) =>
        props.mode === 'info'
            ? props.theme.colors.primary.light
            : props.theme.colors.error.light};
    border-radius: 12px;
    padding: 24px;
    position: relative;
    min-height: 206px;
    max-width: 1280px;
    margin-left: auto;
    margin-right: auto;
    display: flex;
    flex-direction: column;
`;

export const ControlsWrapper = styled.div`
    display: flex;
    margin-top: auto;
`;

export const IconButton = styled.button<ModeProps>`
    border: none;
    background-color: ${({ theme, mode }) =>
        mode === 'info' ? theme.colors.primary.dirt : theme.colors.error.dirt};
    width: 32px;
    height: 32px;
    display: flex;
    align-items: center;
    justify-content: center;
    border-radius: 16px;

    &:last-child {
        margin-left: 18px;
    }
`;

export const CloseButton = styled(IconButton)`
    position: absolute;
    right: 8px;
    top: 8px;
`;

export const Character = styled.img`
    width: 180px;
    height: 180px;
    position: absolute;
    right: 48px;
    top: 50%;
    transform: translate(0, -50%);
`;

export const ActionButton = styled.button<ModeProps>`
    border: none;
    background-color: ${({ theme, mode }) =>
        mode === 'info' ? theme.colors.primary.dirt : theme.colors.error.dirt};
    display: flex;
    align-items: center;
    justify-content: center;
    border-radius: 6px;
    height: 32px;
    padding: 0 12px;
`;

type SkeletonProps = {
    width: number;
    height: number;
    marginVertical: number;
};

export const Skeleton = styled.div<SkeletonProps>(
    ({ theme, width, height, marginVertical }) => `
    background-color: ${theme.colors.primary.dirt};
    width: ${width}px;
    height: ${height}px;
    margin: ${marginVertical}px 0;
    border-radius: 12px;
`
);

export const CloseButtonSkeleton = styled(Skeleton)`
    position: absolute;
    right: 8px;
    top: 8px;
`;
