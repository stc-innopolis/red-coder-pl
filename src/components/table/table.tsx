import dayjs from 'dayjs';
import React from 'react';
import { useTranslation } from 'react-i18next';

import { URLs } from '../../__data__/urls';

import { BodyCell, HeadCell, Wrapper, Skeleton } from './style';

type TableProps = {
    headers: Array<{
        key: string;
        title: string;
        frWidth?: string;
        date?: boolean;
    }>;
    drafts?: boolean;
    data: { id: string; names: { [key: string]: string } }[];
};

export const TableSceleton: React.FC<
    { count: number } & Omit<TableProps, 'data'>
> = ({ headers, count }) => (
    <Wrapper columnsSizes={headers.map((h) => h.frWidth || '1fr')}>
        {headers.map((h, index) => (
            <HeadCell $isLast={index === headers.length - 1} key={h.key}>
                <Skeleton height={16} marginVertical={0} />
            </HeadCell>
        ))}
        {Array.from(new Array(count)).map((o, index) =>
            headers.map((_, hIndex) => (
                <BodyCell
                    to=""
                    $isLast={hIndex === headers.length - 1}
                    even={!(index % 2)}
                    key={`${index}-${hIndex}`}
                >
                    <Skeleton
                        width={Math.random() * 60}
                        height={16}
                        marginVertical={0}
                    />
                </BodyCell>
            ))
        )}
    </Wrapper>
);

export const Table: React.FC<TableProps> = ({ headers, data, drafts }) => {
    const { t, i18n } = useTranslation();

    return (
        <Wrapper columnsSizes={headers.map((h) => h.frWidth || '1fr')}>
            {headers.map((h, index) => (
                <HeadCell $isLast={index === headers.length - 1} key={h.key}>
                    {t(h.title)}
                </HeadCell>
            ))}
            {data.map((d, index) => (
                <React.Fragment key={`${data[headers[0].key]}-${index}`}>
                    {headers.map((h, hIndex) => (
                        <BodyCell
                            to={
                                drafts
                                    ? URLs.editTask.getUrl(d.id)
                                    : URLs.newAnswer.getUrl(d.id)
                            }
                            $isLast={hIndex === headers.length - 1}
                            even={!(index % 2)}
                            key={h.key}
                        >
                            {typeof d[h.key] === 'object'
                                ? d[h.key][i18n.language]
                                : h.date
                                    ? dayjs(d[h.key]).format('DD MMMM YYYY')
                                    : d[h.key]}
                        </BodyCell>
                    ))}
                </React.Fragment>
            ))}
        </Wrapper>
    );
};
