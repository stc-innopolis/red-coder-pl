import styled from '@emotion/styled';
import { Link as ConnectedLink } from 'react-router-dom';

type WrapperProps = {
    columnsSizes: string[];
};

export const Wrapper = styled.div<WrapperProps>`
    display: grid;
    grid-template-columns: ${(props) => props.columnsSizes.join(' ')};
    background: #ffffff;
    box-shadow: 0px 1px 3px rgba(0, 0, 0, 0.25);
    border-radius: 4px;
`;

type HeadCellProps = {
    $isLast: boolean;
};

export const HeadCell = styled.div<HeadCellProps>`
    grid-row-start: 1;
    font-family: 'Roboto';
    font-style: normal;
    font-weight: 400;
    font-size: 16px;
    line-height: 19px;
    padding: 24px;
    text-align: ${(props) => (props.$isLast ? 'end' : 'start')};
`;

type BodyCellProps = HeadCellProps & {
    even: boolean;
};

export const BodyCell = styled(ConnectedLink)<BodyCellProps>(
    (props) => `
    background-color: ${props.even ? '#f5f5f5' : '#ffffff'};
    font-family: 'Roboto';
    font-style: normal;
    font-weight: 400;
    font-size: 18px;
    line-height: 21px;
    padding: 18px;
    color: #000000;
    text-decoration: none;
    text-align: ${props.$isLast ? 'end' : 'start'};
    border-top: 1px solid #E0E0E0;

    
    :hover {
        text-decoration: underline;
    }
`
);

type SkeletonProps = {
    width?: number;
    height: number;
    marginVertical: number;
};

export const Skeleton = styled.div<SkeletonProps>(
    ({ theme, width, height, marginVertical }) => `
    background-color: #cccccc;
    width: ${width ? `${width}px` : '100%'};
    height: ${height}px;
    margin: ${marginVertical}px 0;
    border-radius: 12px;
`
);
