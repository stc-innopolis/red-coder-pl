import styled from '@emotion/styled';

export const Wrapper = styled.div`
    display: flex;
    justify-content: center;
    min-height: 150px;
    align-items: flex-end;
    height: 45vh;
`;

export const DualRing = styled.div(
    ({ theme }) => `
    display: block;
    width: 64px;
    height: 64px;

  &:after {
    content: "";
    display: block;
    width: 46px;
    height: 46px;
    margin: 1px;
    border-radius: 50%;
    border: 5px solid ${theme.colors.primary.main};
    border-color: ${theme.colors.primary.main} transparent ${theme.colors.primary.main} transparent;
    animation: uds-dual-ring-loader 1.2s linear infinite;
  }

  @keyframes uds-dual-ring-loader {
    to {
      transform: rotate(360deg);
    }
  }
`
);
