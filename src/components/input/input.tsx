import React from 'react';

import { InputStyled, Error } from './style';

type ImputProps = {
    error?: string;
} & React.DetailedHTMLProps<
    React.InputHTMLAttributes<HTMLInputElement>,
    HTMLInputElement
>;

export const Input: React.FC<ImputProps> = ({ error, ...props }) => {
    return (
        <>
            <InputStyled {...props} />
            {error && <Error>{error}</Error>}
        </>
    );
};
