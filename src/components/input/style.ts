import styled from '@emotion/styled';

export const InputStyled = styled.input`
    border: 1px solid #cccccc;
    border-radius: 4px;
    height: 28px;
    margin-top: 24px;
    font-family: 'Roboto';
    font-style: normal;
    font-weight: 400;
    font-size: 14px;
    line-height: 16px;
    padding-left: 8px;
    padding-right: 8px;
`;

export const Error = styled.span`
    color: ${(props) => props.theme.colors.error.main};
`;
