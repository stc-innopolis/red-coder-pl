import React, { Suspense } from 'react';
import { ErrorBoundary } from '../error-boundary';
import { PageLoader } from '../page-loader';

export const Lazy = ({ children }) => (
    <ErrorBoundary>
        <Suspense fallback={<PageLoader />}>{children}</Suspense>
    </ErrorBoundary>
);
