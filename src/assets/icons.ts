export { default as avatar } from './icons/user.svg';
export { default as logout } from './icons/log-out.svg';
export { default as close } from './icons/close.svg';
export { default as arrowLeft } from './icons/arrow-left.svg';
export { default as arrowRight } from './icons/arrow-right.svg';
