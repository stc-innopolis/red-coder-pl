export { default as logo } from './pics/logo.png';

import fatalError from './pics/fatal-error.png';
import allGood from './pics/all-good.png';
import askingQuestion from './pics/asking-question.png';
import coding from './pics/coding.png';

export const chars = {
    allGood,
    askingQuestion,
    coding,
    fatalError,
};
