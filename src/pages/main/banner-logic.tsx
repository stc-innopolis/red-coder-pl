import React, { useState } from 'react';
import { getFeatures } from '@ijl/cli';
import { useDispatch, useSelector } from 'react-redux';

import { Banner, BannerSkeleton } from '../../components';
import { actions as bannerLogicAction } from '../../__data__/slices/banner-logic';
import { StoreType } from '../../__data__/store';
import { useBannerDataQuery } from '../../services/api';

const features = getFeatures('main');
const BannerFeature = features['main.banner'];

const BannerLogic: React.FC = () => {
    const [bannerDataIndex, setBannerIndex] = useState<number>(
        Number(BannerFeature?.value)
    );
    const dispatch = useDispatch();
    const showBanner = useSelector(
        (store: StoreType) => store.banner.showBanner
    );
    const { isSuccess } = useBannerDataQuery();

    const bannersData = [
        {
            title: 'Подсказка на сегодня',
            body: 'Подберите задачки под свой уровень сложности, решите её и наращивайте навык',
            icon: 'allGood',
        },
        {
            title: 'Подсказка на завтра',
            body: 'Не забудь сутра выпить кофе',
            icon: 'coding',
        },
        {
            title: 'Подсказка на вчера',
            body: 'Забыл сутра выпить кофе?',
            icon: 'askingQuestion',
        },
    ];

    const handlebannerClose = () => {
        dispatch(bannerLogicAction.hide());
    };

    const handleBannerNextClick = () => {
        setBannerIndex((bannerDataIndex + 1) % bannersData.length);
    };

    const handleBannerPrevClick = () => {
        const minuOne = bannerDataIndex - 1;
        setBannerIndex(minuOne < 0 ? bannersData.length - 1 : minuOne);
    };

    if (!showBanner) {
        return null;
    }

    if (!isSuccess) {
        return <BannerSkeleton />;
    }

    return (
        <Banner
            title={bannersData[bannerDataIndex].title}
            body={bannersData[bannerDataIndex].body}
            icon={bannersData[bannerDataIndex].icon as 'allGood'}
            onClose={handlebannerClose}
            onNext={handleBannerNextClick}
            onPrev={handleBannerPrevClick}
        />
    );
};

export default BannerLogic;
