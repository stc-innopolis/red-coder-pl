import styled from '@emotion/styled';

export const Wrapper = styled.main`
    max-width: 1280px;
    margin: 0 auto;
`;
