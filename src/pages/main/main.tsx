import React, { lazy } from 'react';
import { getFeatures } from '@ijl/cli';
import { useTranslation } from 'react-i18next';
import { useNavigate } from 'react-router-dom';

import {
    PageWrapper,
    Typography,
    Table,
    Lazy,
    TableSceleton,
    Banner,
} from '../../components';
import { URLs } from '../../__data__/urls';
import { useTasksQuery, useDraftsQuery } from '../../services/api';

import { Wrapper } from './style';

const headers = [
    {
        key: 'number',
        title: 'red.coder.main.table.head.number',
        frWidth: '120px',
    },
    { key: 'names', title: 'red.coder.main.table.head.name' },
    {
        key: 'rating',
        title: 'red.coder.main.table.head.rating',
        frWidth: '180px',
    },
    {
        key: 'resolveCount',
        title: 'red.coder.main.table.head.resolveCount',
        frWidth: '150px',
    },
    {
        key: 'views',
        title: 'red.coder.main.table.head.views',
        frWidth: '150px',
    },
    {
        key: 'lastModufy',
        date: true,
        title: 'red.coder.main.table.head.lastModufy',
        frWidth: '240px',
    },
];

const features = getFeatures('main');
const bannerFeature = features['main.banner'];

const BannerLogic = lazy(() => import('./banner-logic'));

export const MainPage = () => {
    const { t } = useTranslation();
    const navigate = useNavigate();
    const { data: tasksAnswer, isLoading, error, isSuccess } = useTasksQuery();
    const {
        data: draftsAnswer,
        isLoading: draftsLoading,
        error: draftsError,
        isSuccess: draftsSuccess,
    } = useDraftsQuery();

    const navigateToCreateTask = () => {
        navigate(URLs.createTask.url);
    };

    return (
        <PageWrapper
            actionButtonText={t('red.coder.main.header.button')}
            action={navigateToCreateTask}
        >
            <Wrapper>
                <Typography.Header1>
                    {t('red.coder.main.header')}
                </Typography.Header1>
                {bannerFeature && (
                    <Lazy>
                        <BannerLogic />
                    </Lazy>
                )}
                <Typography.Header2>
                    {t('red.coder.main.drafts.header')}
                </Typography.Header2>
                {draftsError && (
                    <Banner
                        icon="fatalError"
                        title={t('red.coder.common.mistake')}
                        body={error?.data?.error || t('red.coder.error.common')}
                        mode="error"
                    />
                )}
                {draftsSuccess && <Table drafts headers={headers} data={draftsAnswer?.data} />}
                {draftsLoading && <TableSceleton headers={headers} count={4} />}
                <Typography.Header2>
                    {t('red.coder.main.tasks.header')}
                </Typography.Header2>
                {error && (
                    <Banner
                        icon="fatalError"
                        title={t('red.coder.common.mistake')}
                        body={error?.data?.error || t('red.coder.error.common')}
                        mode="error"
                    />
                )}
                {isLoading && <TableSceleton headers={headers} count={4} />}
                {isSuccess && (
                    <Table headers={headers} data={tasksAnswer?.data} />
                )}
            </Wrapper>
        </PageWrapper>
    );
};
