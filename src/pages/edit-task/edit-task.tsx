import React, { useState } from 'react';
import { useTranslation } from 'react-i18next';

import { PageWrapper, Typography, Markdown, Input } from '../../components';
import { useMonako } from '../../hooks/useMonaco';

import { Papper, Central, MonacoNode } from './style';
import { withTask } from './with-task';

const answer = `
    /**
     * @param {string} string Параметр, который необходимо проверить.
     */
    function checkPolindrom(string) {

    }
`;

const EditTask = () => {
    const { t } = useTranslation();
    const [tr, triggerMonako] = useState(false);
    const { nodeRef, getValue } = useMonako(
        answer,
        tr,
        'javascript',
        (value) => {
            // saveAnswer(value);
        }
    );

    return (
        <PageWrapper>
            <Typography.Header2>
                {t('red.coder.edit.task.header')}
            </Typography.Header2>
            <p>
                Название <Input />
            </p>
            <p>
                Тэги <Input />
            </p>
            <Central>
                <Papper>
                    <MonacoNode ref={nodeRef} />
                </Papper>
                <Papper indent="zero" $height={800}>
                    <Markdown markdown={''} />
                </Papper>
            </Central>
        </PageWrapper>
    );
};

export default withTask(EditTask);
