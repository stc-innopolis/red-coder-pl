import React, { useEffect } from 'react';
import { Navigate, useParams } from 'react-router-dom';

import {
    useCreateTaskMutation,
    useLazyGetTaskDataQuery,
} from '../../services/api';
import { PageLoader } from '../../components';
import { URLs } from '../../__data__/urls';

export const withTask = (Component) => {
    const WrapperComponent = () => {
        const { taskId } = useParams();
        const [trigger, answer] = useCreateTaskMutation();
        const [getTaskData, { data }] = useLazyGetTaskDataQuery();

        useEffect(() => {
            if (!taskId && answer.isUninitialized) {
                trigger(void 0);
            }

            if (taskId && !data) {
                getTaskData(taskId);
            }
        }, [taskId]);

        if (!taskId) {
            if (answer.isSuccess) {
                return (
                    <Navigate
                        replace
                        to={URLs.editTask.getUrl(answer.data.id)}
                    />
                );
            } else {
                return <PageLoader />;
            }
        }

        if (data) {
            return <Component task={data.task} />;
        }

        return null;
    };

    return WrapperComponent;
};
