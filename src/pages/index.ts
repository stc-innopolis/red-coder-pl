import { lazy } from 'react';

export const MainPage = lazy(() => import('./main'));
export const AnswerPage = lazy(() => import('./answer'));
export const EditTask = lazy(() => import('./edit-task'));
