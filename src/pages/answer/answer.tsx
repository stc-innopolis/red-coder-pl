import React, { useState } from 'react';

import { PageWrapper, Typography, Markdown, Input } from '../../components';
import { useMonako } from '../../hooks/useMonaco';

import { Papper, Central, MonacoNode } from './style';

const markdown = `
# Напишите функцию проверки текста на полиндром
## Напишите функцию проверки текста на полиндром
### Напишите функцию проверки текста на полиндром
#### Напишите функцию проверки текста на полиндром
##### Напишите функцию проверки текста на полиндром
###### Напишите функцию проверки текста на полиндром

Блок текста с описанием задачи.
[Ссылка](https://reactjs.org/) на документацию реакта

Не забудьте [зарегистрироваться](/auth) и [поискать](/main) задачки

![react logo](https://tproger.ru/s3/uploads/2016/10/reactmini.png)

![line](https://www.clipartmax.com/png/small/44-446497_lines-clipart-vertical-line-blue-vertical-line-png.png)

> цитата
> цитата
> цитата

\`\`\`javascript
import { useCallback, useRef, useEffect } from 'react';
import { editor as mEditor } from 'monaco-editor';

export const useMonako = (inputData, remountFlag, language: string, onChange = (value: string) => null) => {
    const editorRef = useRef<mEditor.IStandaloneCodeEditor>();
    const nodeRef = useRef(null);
    const remount = useCallback(() => {
        editorRef.current?.dispose();
        editorRef.current = mEditor.create(nodeRef.current, {
            value: inputData,
            theme: 'light' === 'light' ? 'vs' : 'vs-dark',
            language,
        });

        const model = editorRef.current.getModel();
        model.onDidChangeContent(() => {
            const value = model.getValue();
            onChange(value)
        });
    } ,[inputData, nodeRef]);

    useEffect(() => {
        try {
            setTimeout(() => {
                if (nodeRef.current) {
                    remount();

                    () => {
                        editorRef.current.dispose();
                        editorRef.current = null;
                    };
                }
            }, 2);
        } catch (error) {
            console.error(error);
        }
    }, [nodeRef, remountFlag]);

    return {
        nodeRef,
        getValue: () => editorRef?.current?.getValue(),
    };
}

\`\`\`

`;

const answer = `
    /**
     * @param {string} string Параметр, который необходимо проверить.
     */
    function checkPolindrom(string) {

    }
`;

const AnswerPage: React.FC = () => {
    const [tr, triggerMonako] = useState(false);
    const { nodeRef, getValue } = useMonako(
        answer,
        tr,
        'javascript',
        (value) => {
            // saveAnswer(value);
        }
    );

    return (
        <PageWrapper>
            <p>
                Заголовок <Input />
            </p>
            <p>
                Тэги <Input />
            </p>
            <Central>
                <Papper>
                    <Typography.Header2>Полиндром</Typography.Header2>
                    <Markdown markdown={markdown} />
                </Papper>
                <Papper indent="zero" $height={800}>
                    <MonacoNode ref={nodeRef} />
                </Papper>
            </Central>
        </PageWrapper>
    );
};

export default AnswerPage;
