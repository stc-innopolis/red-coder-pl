import styled from '@emotion/styled';

export const Central = styled.div`
    display: flex;
    min-height: 800px;

    @media (max-width: 950px) {
        flex-direction: column;
    }
`;

type IndentProps = {
    indent?: Indent;
};

export const Papper = styled.div<IndentProps & { $height?: number }>`
    width: 50%;
    overflow: auto;
    box-shadow: 0px 4px 14px rgba(0, 0, 0, 0.12);
    ${({ $height }) => $height && `min-height: ${$height}px;`}
    max-height: 900px;
    padding: ${({ indent = 'default' }) => (indent === 'zero' ? 0 : '24px')};

    @media (max-width: 950px) {
        width: 100%;
    }
`;

export const MonacoNode = styled.div`
    width: 100%;
    height: 100%;
    padding-top: 24px;
    background-color: #ffffff;
    z-index: 2;
`;
