import { createApi, fetchBaseQuery } from '@reduxjs/toolkit/query/react';
import { getConfigValue } from '@ijl/cli';

import { BannerData } from '../__data__/model/banner';
import { Request } from '../__data__/model/request';
import { TaskShort } from '../__data__/model/tasks';
import { authToken } from '../utils/token';

enum Tags {
    TASKS = 'TASKS',
    DRAFTS = 'DRAFTS',
}

export const api = createApi({
    reducerPath: 'api',
    baseQuery: fetchBaseQuery({
        baseUrl: getConfigValue('main.red.coder.api'),
        prepareHeaders: (headers) => {
            headers.set('Authorization', `Bearer ${authToken.token}`);
            return headers;
        },
    }),
    tagTypes: [Tags.TASKS, Tags.DRAFTS],
    endpoints: (builder) => ({
        bannerData: builder.query<Request<BannerData[]>, void>({
            query: () => '/banner-data',
            providesTags: [Tags.TASKS, Tags.DRAFTS],
        }),
        tasks: builder.query<Request<TaskShort[]>, void>({
            providesTags: [Tags.TASKS, Tags.DRAFTS],
            query: () => '/v1/tasks',
        }),
        drafts: builder.query<Request<TaskShort[]>, void>({
            providesTags: [Tags.TASKS, Tags.DRAFTS],
            query: () => '/v1/drafts',
        }),
        createTask: builder.mutation<{ id: string }, void>({
            query: () => ({
                url: '/v1/tasks/create',
                method: 'POST',
            }),
            invalidatesTags: [Tags.DRAFTS],
        }),
        getTaskData: builder.query<{ task: unknown }, string>({
            query: (id) => ({
                method: 'POST',
                url: '/v1/task/data',
                body: {
                    id,
                },
            }),
        }),
    }),
});

export const {
    useBannerDataQuery,
    useCreateTaskMutation,
    useLazyGetTaskDataQuery,
    useTasksQuery,
    useDraftsQuery,
} = api;
