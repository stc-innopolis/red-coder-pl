import { describe, test, expect } from '@jest/globals';

import { LOCAL_STORAGE_TOKEN_KEY } from '../../__data__/constants';
import { authToken } from '../token';

describe('check token utils', () => {
    test('success', () => {
        const checkString = 'some token';
        localStorage.setItem(LOCAL_STORAGE_TOKEN_KEY, checkString);

        expect(authToken.token).toBe(checkString);
        expect(authToken.checkToken()).toBe(true);
        authToken.clearToken();
        expect(authToken.checkToken()).toBe(false);
    });
});
