import { LOCAL_STORAGE_TOKEN_KEY } from '../__data__/constants';

export const authToken = {
    get token() {
        return localStorage.getItem(LOCAL_STORAGE_TOKEN_KEY);
    },
    checkToken() {
        return Boolean(localStorage.getItem(LOCAL_STORAGE_TOKEN_KEY));
    },
    clearToken() {
        localStorage.removeItem(LOCAL_STORAGE_TOKEN_KEY);
    },
};
